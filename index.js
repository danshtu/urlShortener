const express = require('express');
const conenctDB = require('./config/db');

const app = express();

// Connect to database
conenctDB();

app.use(express.json({ extended: false})); // Allows to accept json data into our API

// Define Routes
app.use('/', require('./routes/index'));
app.use('/api/url', require('./routes/url'));

const PORT = 5000;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));